import React, { Fragment } from 'react';

import { Input } from "ui"
import { useExampleStore } from "store"

const App: React.FC = () => {

  const [ exampleStore, exampleActions ] = useExampleStore()
  const { text, counter } = exampleStore
  const { incrementCount, changeText } = exampleActions

  return (
    <Fragment>
      <p>Counter: {counter}, text: {text}</p>
      <button onClick={() => incrementCount()}>Click me to increment!</button>
      <button onClick={() => incrementCount()}>I increment too!</button>
      <div>
        <Input value={text} onChange={event => changeText(event.target.value)} />
        <Input value={text} onChange={event => changeText(event.target.value)} />
      </div>
    </Fragment>
  );
}

export default App;
