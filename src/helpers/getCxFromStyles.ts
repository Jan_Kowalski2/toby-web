import classNames from 'classnames/bind'

const mergeCx = (baseCx: Cx, customCx: Cx): Cx => {
    return (...styles: any): string => {
        const classes = [baseCx(styles), customCx(styles)]
        return classes.join(' ')
    }
}

const getCxFromStyles = (styles: any, customStyles: any = {}): Cx => {
    const cx = classNames.bind(styles)
    const customCx: Cx = classNames.bind(customStyles)
    const areCustomStyles =
        customStyles && typeof customStyles === 'object' && Object.keys(customStyles).length > 0

    if (!areCustomStyles) {
        return cx
    }

    return mergeCx(cx, customCx)
}

export default getCxFromStyles
