import StoreClass from "./abstractStore"

interface IExampleStore {
    counter: number,
    text: string
}

const initialState: IExampleStore = {
    counter: 0,
    text: ""
}

interface IExampleActions {
    incrementCount: () => void
    changeText: (text: string) => void
}

class ExampleStore extends StoreClass<IExampleStore, IExampleActions> {
    private static instance: ExampleStore

    public static getInstance() {
        if (!ExampleStore.instance) {
            ExampleStore.instance = new ExampleStore(initialState)
        }

        return ExampleStore.instance
    }

    protected actions: IExampleActions = {
        incrementCount: () => this.incrementCount(),
        changeText: text => this.changeText(text)
    }

    private incrementCount = () => this.setState({ counter: this.state.counter + 1 })

    private changeText = (text: string) => this.setState({ text })
}

export default ExampleStore.getInstance().useStore