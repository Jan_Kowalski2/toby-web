import React from "react"

type Listener<IState> = React.Dispatch<React.SetStateAction<IState>>

abstract class StoreClass<IState, IActions> {
    protected state: IState
    protected abstract actions: IActions
    private listeners: Array<Listener<IState>> = []

    constructor(initialState: IState) {
        this.state = initialState
    }

    public useStore = (): [IState, IActions] => {
        const newListener = React.useState(this.state)[1]

        React.useEffect(() => {
            this.listeners.push(newListener)

            return () => {
                this.listeners = this.listeners.filter(listener => listener !== newListener)
            }
            // eslint-disable-next-line react-hooks/exhaustive-deps
        }, [])

        return [this.state, this.actions]
    }

    protected setState(
        newState: { [key in keyof IState]?: IState[key] }, 
        callback?: (newState: IState) => void
    ) {
        this.state = {...this.state, ...newState }
        this.listeners.forEach(listener => listener(this.state))
        callback && callback(this.state)
    }
}

export default StoreClass