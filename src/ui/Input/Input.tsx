import React from "react"
import styles from "./input.module.css"
import { getStyles } from "helpers"
type InputProps = {
    value: string,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}


const Input = (props: InputProps): JSX.Element => {
    const { value, onChange } = props
    const cx = getStyles(styles)

    return (
        <div className={cx("root")}>
           <input 
                type="text" 
                value={value}
                onChange={event => onChange(event)}
           />
        </div>
    )

    
}

export default Input